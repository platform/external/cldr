---
title: Miscellaneous
---

# Miscellaneous

This section provides translation information about miscellaneous pages in the Survey Tool.
See the subpages, using the ≡ menu at the top of this page. 
